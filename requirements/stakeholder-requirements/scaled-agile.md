# Stakeholder Requirements Derived From RE-related Challenges of Scaled-Agile System Development

In this file, we share our high-level analysis of common requirements-related challenges of scaled agile system development.
Details on our challenges can be found in the following resources:

- Kasauli, R.; Knauss, E.; Horkoff, J.; Liebel, G. and de Oliveira Netoa, F. G.: Requirements engineering challenges and practices in large-scale agile system development. In: Systems and Software, 172, DOI: [10.1016/j.jss.2020.110851](https://doi.org/10.1016/j.jss.2020.110851) (open access), 2020
- Kasauli, R.: Requirements Engineering that Balances Agility of Teams and System-level Information Needs at Scale, [PhD thesis](https://research.chalmers.se/publication/517099), Chalmers, 2020
- Knauss, E.: The missing requirements perspective in large-scale agile system development. In: IEEE Software, vol. 36, pg. 9-13, DOI: [10.1109/MS.2019.2896875](https://doi.org/10.1109/MS.2019.2896875), 2019
- Knauss, E.; Liebel, G.; Horkoff, J.; Wohlrab, R.; Kasauli, R.; Lange, F. and Gildert, P.: T-Reqs: Tool Support for Managing Requirements in Large-Scale Agile System Development: In Proceedings of 26th IEEE International Requirements Engineering Conference (RE'18), [read on ArXive](http://arxiv.org/abs/1805.02769), 2018

<treqs-element id="5f5a94bc107911ecbcadc4b301c00591" type="stakeholder-need">

## C-1.1 Updating and deprecating requirements

Requirements reside between teams on different levels. Teams have different scopes but dependencies exist. It is difficult to establish governance and policing function for shared requirements.

### Current status

Updates of requirements must be proposed to a central role. The process is slow, the central role becomes a bottleneck, and changes that appear non-critical may be omitted.

</treqs-element>

### Related stakeholder Requirements

<treqs-element id="8325982e107911eca2c0c4b301c00591" type="stakeholder-requirement">

#### REQ-C-1.1-1 Allow Developers to share updates

As a member of a XFT (Cross-Functional Team), I want to share new knowledge we learnt about existing requirements during a sprint so that our implementation and the requirements on system level are consistent.
<treqs-link type="addresses" target="5f5a94bc107911ecbcadc4b301c00591" />
<treqs-link type="addresses" target="3adab348b89411ebbf8411e42223e21f" />
</treqs-element>

<treqs-element id="83b7a868107911ecb0ffc4b301c00591" type="stakeholder-requirement">

#### REQ-C-1.1-2 Enable developers to become aware of relevant updates

As a member of a XFT (Cross-Functional Team), I want to be aware of requirements changes that affect my team so that we can pro-actively address dependencies.

<treqs-link type="addresses" target="5f5a94bc107911ecbcadc4b301c00591" />
<treqs-link type="addresses" target="3adab348b89411ebbf8411e42223e21f" />
</treqs-element>

<treqs-element id="761aeb7c107c11ec94e2c4b301c00591" type="information">

### Proposed Solution

Git allows to group changes (e.g. to source code and tests) into commits that then can be pushed to the main branch. T-Reqs allows to manage requirements in exactly the same way. A team pulling the latest changes from the main branch will see conflicts on either of these artefacts as merge conflicts in git.

<treqs-link type="relatesTo" target="8325982e107911eca2c0c4b301c00591" />
<treqs-link type="relatesTo" target="83b7a868107911ecb0ffc4b301c00591" />

</treqs-element>

<treqs-element id="0698f7ac107d11ecb38dc4b301c00591" type="stakeholder-need">

## C-1.2 Access to tooling and requirements

Traditional tools rely on defined change management processes and often do not scale well with respect to parallel users and changes. Development teams find this situation at odds with agile practices and pace.

### Current Status

Often, teams do not have access to tooling and requirements, since licenses are expensive and the number of parallel users is limited.

</treqs-element>

### Related stakeholder Requirements

<treqs-element id="075dbff6107d11ec9f34c4b301c00591" type="stakeholder-requirement">

#### REQ-C-1.2-1 Integrate requirements with developer tools

As a member of a XFT,I want to update system requirements efficiently, without too much overhead, and ideally integrated in the tools I use in my daily work.

<treqs-link type="addresses" target="0698f7ac107d11ecb38dc4b301c00591" />
<treqs-link type="addresses" target="3adab348b89411ebbf8411e42223e21f" />

</treqs-element>

<treqs-element id="080e7468107d11ecb1efc4b301c00591" type="information">

### Proposed Solution

Development teams are used to git, thus T-Reqs provides them with a familiar interface to manipulate requirements. T-Reqs-specific conventions, templates and scripts allow generating specific views and reports for non-technical stakeholders.

<treqs-link type="relatesTo" target="075dbff6107d11ec9f34c4b301c00591" />
</treqs-element>

<treqs-element id="f6b52684107d11eca8aac4b301c00591" type="stakeholder-need">

## C-1.3 Consistent requirements quality

Quality of requirements (i.e. user stories, backlogs) differs (e.g. in detail). This allows best requirements practices for each domain, but reasoning on system level is difficult

### Current status

No appropriate review and alignment process exists that would allow to include an individual team’s way of working.

</treqs-element>

### Related stakeholder Requirements

<treqs-element id="f772ec6e107d11ecb1f5c4b301c00591" type="stakeholder-requirement">

#### Req-C-1.3-1 Quality assurance of requirements of different teams

As a system manager, I want to make sure that proposed updates to requirements are of good quality, do not conflict with each other, or with the product mission.

<treqs-link type="addresses" target="f6b52684107d11eca8aac4b301c00591" />
</treqs-element>
<treqs-element id="f818f122107d11ec8d39c4b301c00591" type="information">

### Proposed Solution

Many organisations that rely on git are also using gerrit or similar tools to manage reviews (e.g. of source code). T-Reqs organizes requirements in a way that allows to do that with requirements as well.

<treqs-link type="relatesTo" target="f772ec6e107d11ecb1f5c4b301c00591" />
</treqs-element>

<treqs-element id="5f42b0bc107f11ecb81dc4b301c00591" type="stakeholder-need">

## C-1.4 Managing experimental requirements

When exploring new functionality or product ideas, experimental requirements need to be treated differently from stable requirements. Still, they need to be captured and later integrated in the system view.

### Current status

The requirements database is cloned before experimenting and must be blocked for other changes when the clone is eventually be ported back.

</treqs-element>

### Related stakeholder Requirements

<treqs-element id="62812fb0107f11ecb014c4b301c00591" type="stakeholder-requirement">

#### Req-C-1.4-1 Support what-if analysis and experimenting with new requirements

As member of an experimenting team, I want to experiment with new requirements and features so that I can better assess their business value and cost. This must not affect existing requirements during the experiment or block the requirements database afterwards.

<treqs-link type="addresses" target="5f42b0bc107f11ecb81dc4b301c00591" />

</treqs-element>

<treqs-element id="635954d0107f11ec8525c4b301c00591" type="information">

### Proposed Solution

Git allows creating branches to experiment with requirements, but also with models and source code.
Git merge and gerrit help to merge branches, without blocking the main database.
Merge conflicts will directly relate to requirements conflicts, since requirements are stored line-wise.

<treqs-link type="relatesTo" target="62812fb0107f11ecb014c4b301c00591" />

</treqs-element>

<treqs-element id="d7569fe0108011ecaf28c4b301c00591" type="stakeholder-need">

## C-1.5 Create and maintain traces

In continuous integration and delivery, agile teams struggle to provide sufficient tracing to allow determining the status of individual features.

### Current status

Tracing does not offer direct value to agile teams and is not integrated in their workflows.
</treqs-element>

### Related stakeholder Requirements

<treqs-element id="d80405ea108011ec9376c4b301c00591" type="stakeholder-requirement">

#### Req-C-1.5-1 Provide traceability between requirements, change sets and tests

As a member of a XFT,I want to maintain traces between requirements, change sets, and tests in a way that is integrated with my natural workflow and enables valuable feedback.

<treqs-link type="addresses" target="d7569fe0108011ecaf28c4b301c00591" />
</treqs-element>
<treqs-element id="d8bc68b0108011ec9c01c4b301c00591" type="information">

### Proposed Solution

Git automatically links changes of code and requirements in commits.
T-Reqs adds conventions, templates, and scripts for additional finer-grained tracing.
Providing cross-functional teams with feedback based on tracing information can further motivate good trace link quality.

<treqs-link type="relatesTo" target="d80405ea108011ec9376c4b301c00591" />
</treqs-element>

<treqs-element id="e7dc6f70108011eca1a8c4b301c00591" type="stakeholder-need">

## C-1.6 Plan verification and validation based on requirements

When requirements updates by individual teams are not shared on system level, it is impossible to plan verification and validation pro-actively.

### Current status

Requirements changes are difficult to share and the need to update complex system testing infrastructure may surface late.

</treqs-element>
<treqs-element id="e88896ce108011ec9493c4b301c00591" type="stakeholder-requirement">

#### Req-C-1.6-1 Enable system level roles to become aware of relevant updates

As a test architect or system manager, I want to be aware of new requirements for the test infrastructure early on so that I can plan verification and validation pro-actively.

<treqs-link type="addresses" target="e7dc6f70108011eca1a8c4b301c00591" />
</treqs-element>

<treqs-element id="e9238b20108011ecae55c4b301c00591" type="information">

### Proposed Solution

T-Reqs suggests a suitable review process via typical tools in the git ecosystem (e.g. merge/pull requests, gerrit) that has proven to spread information about critical changes effectively between key stakeholders.
Ease of use makes it more likely that teams share requirements updates in a timely manner.

<treqs-link type="relatesTo" target="e88896ce108011ec9493c4b301c00591" />
</treqs-element>
