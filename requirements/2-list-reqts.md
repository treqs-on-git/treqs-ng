# TReqs List

In this section, we describe refined requirements for [Req2: List Treqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements).

## Detailed requirements for treqs list

<treqs-element id="a0820e06-9614-11ea-bb37-0242ac130002" type="requirement" >

### Req-2.1: Parameters and default output of treqs list

```bash
Usage: treqs list [OPTIONS] [FILENAME]...
```

  List treqs elements in the specified folder

#### Options

```--type TEXT``` Limit action to specified treqs element type

```--uid TEXT``` Limit action to treqs element with specified id

```--outlinks / --no-outlinks``` Print outgoing tracelinks  [default: False]

```--inlinks / --no-inlinks``` Print incoming tracelinks  [default: False]

```--followlinks BOOLEAN```  List treqs elements recursively by following links (only with plantuml option). [default: False]

```--verbose / --no-verbose``` Print verbose output instead of only the most important messages.  [default: False]

```--plantuml / --no-plantuml```Generates a PlantUML diagram from the treqs elements.  [default: no-plantuml]

```--recursive BOOLEAN``` List treqs elements recursively in all subfolders.

```--help``` Show this message and exit.

If `type` is omitted, all treqs elements, regardless of type, will be returned.

If `uid` is omitted, all treqs elements, regardless of uid, will be returned.

The default value for `recursive` is `True`. Unless otherwise specified, treqs shall list elements recursively.

`FILENAME` can either provide a directory or a file. If `FILENAME` is omitted, treqs defaults to `.`, i.e. the current working directory.

#### Links to

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)
</treqs-link>

<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591">

- addresses: [REQ-CT-P1.4-1 Support upfront traceability](stakeholder-requirements/collaborative-traceability.md#req-ct-p14-1-support-upfront-traceability)
</treqs-link>

</treqs-element>
<treqs-element id="63ef8bfa76ae11ebb811cf2f044815f7" type="requirement">

### Req-2.2 Information listed by treqs list

When listing treqs elements, treqs shall list the following information as a markdown table:

- the element type,
- the label (i.e. the first non-empty line of a treqs element),
- the id of the element (the UID provided by the treqs create command), and
- the file and location (line number) where the element start tag is located.

The file and the line number shall be displayed in the same column separated by a colon in order for IDEs to interpret it.

>**Example:**
>
>```bash
>treqs list ./requirements/2-list-reqts.md
>```
>
>| UID | Type | Label | File:Line |
>| :--- | :--- | :--- | :--- |
>| a0820e06-9614-11ea-bb37-0242ac130002 | requirement | ### Req-2.1: Parameters and default output of treqs list | ./requirements/2-list-reqts.md:5 |
>| 63ef8bfa76ae11ebb811cf2f044815f7 | requirement | ### Req-2.2 Information listed by treqs list | ./requirements/2-list-reqts.md:50 |
>| 437f09c6-9613-11ea-bb37-0242ac130002 | requirement | ### Req-2.3 Filter by type  | ./requirements/2-list-reqts.md:89 |
>| abc40962a23511eba9dca7925d1c5fe9 | information | Note that the type should usually be defined in the TTIM. treqs list does however not check for this to be the case. Use treqs check instead to make sure that all types are consistent with the TTIM. | ./requirements/2-list-reqts.md:108 |
>| a0820b4a-9614-11ea-bb37-0242ac130002 | requirement | ### Req-2.4 Filter by ID | ./requirements/2-list-reqts.md:113 |
>| bc89e02a76c811ebb811cf2f044815f7 | requirement | ### Req-2.5 List all elements in a file | ./requirements/2-list-reqts.md:133 |
>| 638fa22e76c911ebb811cf2f044815f7 | requirement | ### Req-2.6 List treqs elements in a directory | ./requirements/2-list-reqts.md:148 |
>| 1595ed20a27111eb8d3991dd3edc620a | requirement | ### Req-2.7 List outgoing tracelinks | ./requirements/2-list-reqts.md:165 |
>| d9e68f9aa27b11eb8d3991dd3edc620a | requirement | ### Req-2.8 List incoming tracelinks | ./requirements/2-list-reqts.md:218 |

#### Links to

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)

</treqs-link>
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />

</treqs-element>

<treqs-element id="437f09c6-9613-11ea-bb37-0242ac130002" type="requirement" >

### Req-2.3 Filter by type

TReqs shall allow to list treqs elements within a specified folder/file that have a specific type (e.g. requirement, test, quality requirement).

#### Links to

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)

</treqs-link>
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />

> Example:
>
>```bash
>treqs list --type=requirement folderName
>```

</treqs-element>

<treqs-element id="abc40962a23511eba9dca7925d1c5fe9" type="information">

Note that the type should usually be defined in the TTIM. treqs list does however not check for this to be the case. Use treqs check instead to make sure that all types are consistent with the TTIM.

<treqs-link type="relatesTo" target="437f09c6-9613-11ea-bb37-0242ac130002"/>

</treqs-element>

<treqs-element id="a0820b4a-9614-11ea-bb37-0242ac130002" type="requirement" >

### Req-2.4 Filter by ID

TReqs shall allow to list treqs elements within a specified folder/file that have a specific ID.

#### Links to

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)

</treqs-link>

<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />

<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591" />

>**Example:**
>
>```bash
>treqs list --uid "35590bca-960f-11ea-bb37-0242ac130002" folderName
>```

</treqs-element>

<treqs-element id="bc89e02a76c811ebb811cf2f044815f7" type="requirement">

### Req-2.5 List all elements in a file

If a file is given, treqs will list all treqs elements in that file.

#### Links to

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)

</treqs-link>

<treqs-link type="relatesTo" target="a0820e06-9614-11ea-bb37-0242ac130002" />
<treqs-link type="relatesTo" target="48f619d6dfae11ef97f0d89ef374263e" />

</treqs-element>

<treqs-element id="638fa22e76c911ebb811cf2f044815f7" type="requirement">

### Req-2.6 List treqs elements in a directory

If a directory is given, treqs will list all treqs elements in all files of this directory.
If the recursive parameter is given, treqs will list also all treqs elements in all subdirectories of the given directory.

#### Links to

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)
</treqs-link>

<treqs-link type="relatesTo" target="bc89e02a76c811ebb811cf2f044815f7" />

<treqs-link type="relatesTo" target="a40a5ce0dfb011efbe55d89ef374263e"/>

</treqs-element>

<treqs-element id="1595ed20a27111eb8d3991dd3edc620a" type="requirement">

### Req-2.7 List outgoing tracelinks

Optionally, treqs list shall list the outgoing tracelinks of each treqs element. This list shall be integrated in the table of the list command. Labels and locations (filename and line number) of target treqs-elements shall be shown, if available (i.e., the current list command includes the file in which the target is specified).

> **Example:**
>
> The following command advises TReqs to only travers a specific file.
>
>```bash
>treqs list --outlinks --uid "1595ed20a27111eb8d3991dd3edc620a" ./requirements/2-list-reqts.md
>```
>
>The output for the command above therefore does only include information from that specific file, limiting information for example about the parent requirement, since the parent of this requirement is specified in a different file.
>
>| UID | Type | Label | File:Line |
>| :--- | :--- | :--- | :--- |
>| 1595ed20a27111eb8d3991dd3edc620a | requirement | ### Req-2.7 List outgoing tracelinks | ./requirements/2-list-reqts.md:167 |
>| --outlink--> (35590bca-960f-11ea-bb37-0242ac130002) | hasParent | Target: Target treqs element not found. Has the containing file been included in the scope? | -- |
>| --outlink--> (63ef8bfa76ae11ebb811cf2f044815f7) | relatesTo | Target: ### Req-2.2 Information listed by treqs list | ./requirements/2-list-reqts.md:50 |
>| --outlink--> (d9e68f9aa27b11eb8d3991dd3edc620a) | relatesTo | Target: ### Req-2.8 List incoming tracelinks | ./requirements/2-list-reqts.md:220 |
>| --outlink--> (1e9885f69d3311eb859fc4b301c00591) | addresses | Target: Target treqs element not found. Has the containing file been included in the scope? | -- |
>| --outlink--> (54a4e59a9d3311ebb4d2c4b301c00591) | addresses | Target: Target treqs element not found. Has the containing file been included in the scope? | -- |
>
>At the same time, the following list command includes all requirements and therefore produces the desired result:
>
>```bash
>treqs list --outlinks --uid "1595ed20a27111eb8d3991dd3edc620a" ./requirements
>```
>
>| UID | Type | Label | File:Line |
>| :--- | :--- | :--- | :--- |
>| 1595ed20a27111eb8d3991dd3edc620a | requirement | ### Req-2.7 List outgoing tracelinks | requirements/2-list-reqts.md:172 |
>| --outlink--> (35590bca-960f-11ea-bb37-0242ac130002) | hasParent | Target: ## Req-2: List TReqs Elements | requirements/treqs-system-requirements.md:113 |
>| --outlink--> (63ef8bfa76ae11ebb811cf2f044815f7) | relatesTo | Target: ### Req-2.2 Information listed by treqs list | requirements/2-list-reqts.md:50 |
>| --outlink--> (d9e68f9aa27b11eb8d3991dd3edc620a) | relatesTo | Target: ### Req-2.8 List incoming tracelinks | requirements/2-list-reqts.md:224 |
>| --outlink--> (1e9885f69d3311eb859fc4b301c00591) | addresses | Target: #### REQ-CT-P1.4-1 Support upfront traceability | requirements/stakeholder-requirements/collaborative-traceability.md:72 |
>| --outlink--> (54a4e59a9d3311ebb4d2c4b301c00591) | addresses | Target: #### REQ-CT-P1.4-2 Support tracelink maintenance | requirements/stakeholder-requirements/collaborative-traceability.md:81 |

#### Links to

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)
</treqs-link>

<treqs-link type="relatesTo" target="63ef8bfa76ae11ebb811cf2f044815f7">

- relatesTo: [Req-2.2 Information listed by treqs list](#req-22-information-listed-by-treqs-list)
</treqs-link>

<treqs-link type="relatesTo" target="d9e68f9aa27b11eb8d3991dd3edc620a">

- relatesTo: [Req-2.8 List incoming tracelinks](#req-28-list-incoming-tracelinks)
</treqs-link>

<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591">

- addresses: [REQ-CT-P1.4-1 Support upfront traceability](stakeholder-requirements/collaborative-traceability.md#req-ct-p14-1-support-upfront-traceability)
</treqs-link>

<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591">

- addresses: [REQ-CT-P1.4-2 Support tracelink maintenance](stakeholder-requirements/collaborative-traceability.md#req-ct-p14-2-support-tracelink-maintenance)
</treqs-link>

<treqs-link type="addresses" target="d80405ea108011ec9376c4b301c00591">

- addresses: [Req-C-1.5-1 Provide traceability between requirements, change sets and tests](stakeholder-requirements/scaled-agile.md#req-c-15-1-provide-traceability-between-requirements-change-sets-and-tests)
</treqs-link>

</treqs-element>

<treqs-element id="d9e68f9aa27b11eb8d3991dd3edc620a" type="requirement">

### Req-2.8 List incoming tracelinks

Optionally, treqs list shall list the incoming tracelinks of each treqs element. This list shall be integrated in the table of the list command. Labels and locations (filename and line number) of target treqs elements shall be shown, if available (i.e. the current list command includes the file in which the target is specified).

> **Example:**
>
>```bash
>treqs list --inlinks --uid "d9e68f9aa27b11eb8d3991dd3edc620a" ./requirements
>```
>
>| UID | Type | Label | File:Line |
>| :--- | :--- | :--- | :--- |
>| d9e68f9aa27b11eb8d3991dd3edc620a | requirement | ### Req-2.8 List incoming tracelinks | requirements/2-list-reqts.md:224 |
>| --inlink--> (1595ed20a27111eb8d3991dd3edc620a) | relatesTo | Source: ### Req-2.7 List outgoing tracelinks | requirements/2-list-reqts.md:172 |

#### Links to

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)

</treqs-link>

<treqs-link type="relatesTo" target="63ef8bfa76ae11ebb811cf2f044815f7" />

<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591">

- addresses: [REQ-CT-P1.4-1 Support upfront traceability](stakeholder-requirements/collaborative-traceability.md#req-ct-p14-1-support-upfront-traceability)

</treqs-link>

<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591">

- addresses: [REQ-CT-P1.4-2 Support tracelink maintenance](stakeholder-requirements/collaborative-traceability.md#req-ct-p14-2-support-tracelink-maintenance)

</treqs-link>
<treqs-link type="addresses" target="d80405ea108011ec9376c4b301c00591">

- addresses: [Req-C-1.5-1 Provide traceability between requirements, change sets and tests](stakeholder-requirements/scaled-agile.md#req-c-15-1-provide-traceability-between-requirements-change-sets-and-tests)

</treqs-link>

</treqs-element>

<treqs-element id="6473290ed22e11eeadb88de628732e03" type="requirement">

### Req-2.9 Generate PlantUML diagram

Optionally, `treqs list` shall print out its output in PlantUML format, instead of the default Markdown-table format.
The PlantUML should be valid and should contain the exact same information in the corressponding `treqs list` i.e. any
of the options that can be used for `treqs list` must be preserved and used in the generation of the PlantUML information.

> **Example:**
>
>```bash
>treqs list --plantuml --inlinks --uid "d9e68f9aa27b11eb8d3991dd3edc620a" ./requirements
>```
>
> **Output:**
>
```plantuml
@startuml
map "**### Req-2.8 List incoming tracelinks**" as d9e68f9aa27b11eb8d3991dd3edc620a {
uid => ""d9e68f9aa27b11eb8d3991dd3edc620a""
type => //requirement//
location => requirements/\n2-list-reqts.md:232
}
map "**### Req-2.7 List outgoing tracelinks**" as 1595ed20a27111eb8d3991dd3edc620a {
uid => ""1595ed20a27111eb8d3991dd3edc620a""
type => //requirement//
location => requirements/\n2-list-reqts.md:171
}
1595ed20a27111eb8d3991dd3edc620a --> d9e68f9aa27b11eb8d3991dd3edc620a : relatesTo
@enduml
```

#### Links to

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)

</treqs-link>
<treqs-link type="relatesTo" target="63ef8bfa76ae11ebb811cf2f044815f7" />
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591" />
<treqs-link type="addresses" target="ba75e2deb86d11ebbf8411e42223e21f" />

</treqs-element>

<treqs-element id="10302d4ee7d111efb4071c697aa14cc2" type="requirement">

### REQ-2.10: Follow links recursively

``treqs list`` shall be able follow links recursively, if the option ``--followlinks`` is set to ``True``.
If no further filter is given (such as <treqs-link type="relatesTo" target="437f09c6-9613-11ea-bb37-0242ac130002"> [Req-2.3 Filter by type](2-list-reqts.md#req-23-filter-by-type) </treqs-link> or <treqs-link type="relatesTo" target="a0820b4a-9614-11ea-bb37-0242ac130002"> [Req-2.4 Filter by ID](#req-24-filter-by-id)</treqs-link>), this should not affect the output, since all links and elements in scope will already be listed.
If neither inlinks nor outlinks are listed (see <treqs-link type="relatesTo" target="1595ed20a27111eb8d3991dd3edc620a">[Req-2.7 List outgoing tracelinks](#req-27-list-outgoing-tracelinks)</treqs-link> and  <treqs-link type="relatesTo" target="d9e68f9aa27b11eb8d3991dd3edc620a">[Req-2.8 List incoming tracelinks](#req-28-list-incoming-tracelinks)</treqs-link>), this should not affect the output, since no links that could be followed are listed.

In all other cases, treqs list should follow the links, list the linked element as well as its in- or outlinks, depending on which options have been given.
For these links, again the linked elements as well as their in- or outlinks should be listed.

This information shall only be listed in formats that can represent complex relationships.
Thus, this option shall not affect the normal out put of treqs (see <treqs-link type="relatesTo" target="63ef8bfa76ae11ebb811cf2f044815f7">[Req-2.2 Information listed by treqs list](#req-22-information-listed-by-treqs-list)</treqs-link>), but it shall affect the output of treqs list with the ``--plantuml`` option (see <treqs-link type="relatesTo" target="6473290ed22e11eeadb88de628732e03">[Req-2.9 Generate PlantUML diagram](#req-29-generate-plantuml-diagram)</treqs-link>).
</treqs-element>
