# TReqs Check

In this section we describe refined requirements of [Req-5: Check Types and Tracelinks](treqs-system-requirements.md#req-5-check-types-and-tracelinks).

## Detailed requirements for treqs check

<treqs-element id="c5402c1a919411eb8311f018989356c1" type="requirement">

### Req-5.0 Parameters and default output of treqs check

```txt
Usage: treqs check [OPTIONS] [FILENAME]...

Checks for consistency of treqs elements.
```

#### Options

```--recursive BOOLEAN``` Check treqs elements recursively in all subfolders.

```--ttim TEXT``` Path to a type and traceability information model (TTIM) in json format.

```--verbose / --no-verbose``` Print verbose output instead of only the most important messages. [default: False]

```--help``` Show this message and exit

The default value for recursive is `True`. Unless otherwise specified, treqs shall list elements recursively.

`FILENAME` can either provide a directory or a file. If `FILENAME` is omitted, treqs defaults to `.`, i.e. the current working directory.

`TTIM` points to a `YML` file that contains a type and trace information model (TTIM). The default is `./ttim.yml`. Currently, the only relevant information in the TTIM file is an array containing all types and their relationships. Consult the sample file for a commented usage example.

#### Links to

<treqs-link type="hasParent" target="8cedc2a5509d11edbe22c9328ceec9a7">

- [Req-5: Check Types and Tracelinks](treqs-system-requirements.md#req-5-check-types-and-tracelinks).

</treqs-link>

<treqs-link type="addresses" target="7438f4c69d3011ebb63fc4b301c00591" />

<treqs-link type="relatesTo" target="3d0063ae9d3511eb898ec4b301c00591" />

<treqs-link type="addresses" target="17927fb4b89511ebbf8411e42223e21f" />

</treqs-element>

<treqs-element id="7ffb9ea8dfaf11ef9d72d89ef374263e" type="requirement">

### REQ-5.1.1: Allow to check a specific file

If given a specific file as input, treqs check shall only check the treqs-elements in this file.

<treqs-link type="relatesTo" target="48f619d6dfae11ef97f0d89ef374263e" />

</treqs-element>

<treqs-element id="c30a2aeadfaf11ef8751d89ef374263e" type="requirement">

### REQ-5.1.2: Allow to check files in a specific directory

If given a specific directory, treqs checks treqs-elements in all valid files in this directory.

<treqs-link type="relatesTo" target="a40a5ce0dfb011efbe55d89ef374263e"/>

</treqs-element>

<treqs-element id="a0ae1162a72711ef80988adebfb72d7c" type="requirement">

### REQ-5.1 Maintain Type and Traceability Information model in Product Repository

TReqs shall maintain a Type and Traceability Information model in the product repository as a configuration file.
This file will be used to define:

- types that treqs-elements may have
- trace-links that treqs-elements of certain types may have
- trace-links that treqs-elements of certain types must have
- targets (=treqs-elements) that trace-links of certain types may have

The type and traceability information model configuration file must have the following properties:

1. Allow comments

<treqs-link type="addresses" target="3d0063ae9d3511eb898ec4b301c00591"/>
<treqs-link type="addresses" target="7438f4c69d3011ebb63fc4b301c00591"/>
<treqs-link type="addresses" target="0c4f3b2a101911ecaa7bc4b301c00591"/>
<treqs-link type="addresses" target="0cf77fb0101911ecb635c4b301c00591"/>

</treqs-element>

<treqs-element id="36c9dc08a72811efa8f78adebfb72d7c" type="information">

#### Possible solution for Req-5.x

YAML files are a good solution for a configuration files.
In contrast to JSON, they allow comments.
They are also very typical in CI and git environments.

<treqs-link type="relatesTo" target="a0ae1162a72711ef80988adebfb72d7c"/>

</treqs-element>

<treqs-element id="3cff0d2a919511eb8becf018989356c1" type="requirement">

### Req-5.2 Check for unrecognized or empty/missing types

When checking treqs elements, treqs shall report elements that have types not listed in the TTIM or empty/missing types. Additionally, treqs shall report the file and line in which this violation occurred.

> **Example:**
>
>```markdown
>| Element e770de36920911eb9355f018989356c1 | Element has an unrecognized type: non-existing-type | ./tests/test_data/5-test-faulty-types-and-links.md:2 |
>```

#### Links to

<treqs-link type="hasParent" target="8cedc2a5509d11edbe22c9328ceec9a7">

- [Req-5: Check Types and Tracelinks](treqs-system-requirements.md#req-5-check-types-and-tracelinks).

</treqs-link>
</treqs-element>

<treqs-element id="951ecc70919511eb978ff018989356c1" type="requirement">

### Req-5.3 Check for unrecognized or missing link types

When checking treqs links, treqs shall report links that have types not listed for the parent element in the TTIM, or empty/missing types. Additionally, treqs shall report the file in which this violation occurred.

> **Example:**
>
>```markdown
>| Element 2c600896920a11ebbb6ff018989356c1 | Unrecognised link type relatesToo within element of type requirement. | ./tests/test_data/5-test-faulty-types-and-links.md:9 |
>```

#### Links to

<treqs-link type="hasParent" target="8cedc2a5509d11edbe22c9328ceec9a7">

- [Req-5: Check Types and Tracelinks](treqs-system-requirements.md#req-5-check-types-and-tracelinks).

</treqs-link>
<treqs-link type="addresses" target="d15209ca9d3211eba2e0c4b301c00591" />
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591" />
<treqs-link type="addresses" target="17927fb4b89511ebbf8411e42223e21f" />
</treqs-element>

<treqs-element id="b4d30bec919711eba4e1f018989356c1" type="requirement">

### Req-5.4 Check for missing links

When checking treqs elements, treqs shall report links that are required according to the TTIM, but missing in the element. Additionally, treqs shall report the file in which this violation occurred.

> **Example:**
>
>```markdown
>| Element afca2be7e0b711eb9b8d7085c2221ca0 |  Required links missing: ['tests'] | ./tests/test_data/7-test-faulty-inlinks-outlinks.md:28 |
>```

#### Links to

<treqs-link type="hasParent" target="8cedc2a5509d11edbe22c9328ceec9a7">

- [Req-5: Check Types and Tracelinks](treqs-system-requirements.md#req-5-check-types-and-tracelinks).

</treqs-link>
<treqs-link type="addresses" target="d15209ca9d3211eba2e0c4b301c00591" />
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591" />
<treqs-link type="addresses" target="17927fb4b89511ebbf8411e42223e21f" />
</treqs-element>

<treqs-element id="a787f20c223011ecb43ef018989356c1" type="requirement">

### Req-5.5: Check missing ids

When checking treqs elements, treqs shall report if an id is missing or empty. Additionally, treqs shall report the type of the element, as well as the file and line in which this violation occurred.

> **Example:**
>
>```markdown
>| Element type requirement | Element does not have an id. | ./tests/test_data/5-test-faulty-types-and-links.md:23 |
>```

**Links to:**
<treqs-link type="hasParent" target="8cedc2a5509d11edbe22c9328ceec9a7">

- [Req-5: Check Types and Tracelinks](treqs-system-requirements.md#req-5-check-types-and-tracelinks).

</treqs-link>
</treqs-element>

<treqs-element id="a787f342223011ecb43ef018989356c1" type="requirement">

### Req-5.6: Check duplicate ids

When checking treqs elements, treqs shall report if duplicate ids exist. This shall also work across files, as long as the relevant files are included in the specified directory for the check. Additionally, treqs shall report the file and line in which this violation occurred.

> **Example:**
>
>```markdown
>| Element 940f4d62920a11eba034f018989356c1 | Element id is duplicated. | ./tests/test_data/5-test-faulty-types-and-links.md:29 |
>```

#### Links to

<treqs-link type="hasParent" target="8cedc2a5509d11edbe22c9328ceec9a7">

- [Req-5: Check Types and Tracelinks](treqs-system-requirements.md#req-5-check-types-and-tracelinks).

</treqs-link>
</treqs-element>

<treqs-element id="a787f3a6223011ecb43ef018989356c1" type="requirement">

### Req-5.7: Check for non-existent referenced elements

When checking treqs links, treqs shall report if no target element with the target id is found. This shall also work across files, as long as the relevant files are included in the specified directory for the check. Additionally, treqs shall report the file and line in which this violation occurred.

> **Example:**
>
>```markdown
>| Element 4f5bcadad45711eb9de4f018989356c1 | Element references non-existent element with id inexistent_target_id | ./tests/test_data/5-test-faulty-types-and-links.md:45 |
>```

#### Links to

<treqs-link type="hasParent" target="8cedc2a5509d11edbe22c9328ceec9a7">

- [Req-5: Check Types and Tracelinks](treqs-system-requirements.md#req-5-check-types-and-tracelinks).

</treqs-link>
</treqs-element>

<treqs-element id="a787f400223011ecb43ef018989356c1" type="requirement">

### Req-5.8 Check for wrong link types

When checking treqs links, treqs shall report if the link type exists in the TTIM with a specified target type, but the referenced element has a different type. Additionally, treqs shall report the file and line in which this violation occurred.

> **Example:**
>
>```markdown
>| Element 3cabd686d45811ebaaeef018989356c1 | 'addresses' link to element 4f5bcadad45711eb9de4f018989356c1 needs to point to a stakeholder-requirement, but points to a requirement instead. | ./tests/test_data/5-test-faulty-types-and-links.md:51 |
>```

#### Links to

<treqs-link type="hasParent" target="8cedc2a5509d11edbe22c9328ceec9a7">

- [Req-5: Check Types and Tracelinks](treqs-system-requirements.md#req-5-check-types-and-tracelinks).

</treqs-link>
</treqs-element>

<treqs-element id="d00b0858628a11ee98f915f1b0e25002" type="requirement">

### Req-5.9: Support targeting multiple link types

The Traceability and Type Infomration Model (TTIM) file (`ttim.yaml`) allows links to target multiple types. The `check` command interprets the `target` property of `links` as either a key-value pair:

```yaml
- name: requirement
  links:
  - type: hasParent
    target: requirement
```

or a list of possible target types for that link:

```yaml
- name: requirement
  links:
  - type: hasParent
    target:
      - requirement
      - system-requirement
```

Semantically, this means that a `requirement` can `hasParent` of types `requirement` or `system-requirement` (or both).

#### Links to

<treqs-link type="hasParent" target="8cedc2a5509d11edbe22c9328ceec9a7">

- [Req-5: Check Types and Tracelinks](treqs-system-requirements.md#req-5-check-types-and-tracelinks).
</treqs-link>

</treqs-element>
