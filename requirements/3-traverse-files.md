# TReqs File Handling

## Detailed list of requirements about handling files in TReqs

<treqs-element id="48f619d6dfae11ef97f0d89ef374263e" type="requirement">

### REQ-3.1: Extract all treqs-elements from a given file.

TReqs shall be able to extract treqs-elements from a given text file.

Typical file types are text files (README.txt), markdown files (requirements.md), or source files (main.java, test_file_traverser.py).

<treqs-link type="relatesTo" target="51928136509311edbe22c9328ceec9a7"/>
<treqs-link type="relatesTo" target="571a0556509411edbe22c9328ceec9a7"/>
<treqs-link type="relatesTo" target="35590bca-960f-11ea-bb37-0242ac130002"/>
<treqs-link type="relatesTo" target="8cedc2a5509d11edbe22c9328ceec9a7"/>

</treqs-element>

<treqs-element id="a40a5ce0dfb011efbe55d89ef374263e" type="requirement">

### REQ-3.2: Extract treqs-elements from all files in a directory

TReqs shall be able to extract all treqs-elements for all files in a given directory.
Optionally, it shall be possible to speficy whether TReqs should recursively check also all requirements of files in sub-directories.
If recursive behavior is not specified, TReqs shall work recursively.

<treqs-link type="relatesTo" target="48f619d6dfae11ef97f0d89ef374263e"/>
<treqs-link type="relatesTo" target="35590bca-960f-11ea-bb37-0242ac130002"/>
<treqs-link type="relatesTo" target="8cedc2a5509d11edbe22c9328ceec9a7"/>

</treqs-element>

<treqs-element id="4b2829b6dfb111ef9c8dd89ef374263e" type="requirement">

### REQ-3.3: Ignore files specified in ``.treqs-ignore``

TReqs shall not process files specified in ``.treqs-ignore``.
TReqs shall assume a similar syntax as ``.gitignore``.

</treqs-element>

<treqs-element id="4d8be51c-df13-41e9-ac02-00ffa31c3b80" type="requirement">

### Req-3.4 Fault Tolerant File Traverser for Uninterrupted TReqs Execution

The file traverser must implement fault tolerance mechanisms to ensure that the execution of TReqs is not abruptly halted due to errors. Instead of stopping execution when any kind of error occurs, the traverser should handle errors gracefully. This means that non-critical errors should be caught and logged without affecting the uninterrupted operation of TReqs, particularly in cases where TReqs do not need to react to the error.

#### Links to

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)

</treqs-link>
<treqs-link type="relatesTo" target="bc89e02a76c811ebb811cf2f044815f7" />
<treqs-link type="relatesTo" target="638fa22e76c911ebb811cf2f044815f7" />

</treqs-element>

<treqs-element id="ab59f9c4a71111efaf2ed89ef374263e" type="requirement">

### REQ-3.5: Ignore invalid XML for non-treqs elements

TReqs shall be able to extract treqs-element and treqs-link from files where XML opening and closing tags are used, such as programming languages like Java, PlantUML, and Kotlin.
At the same time, when ignoring invalid xml, treqs should still provide valid line numbers for the elements it extracts.

**Links to:**

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)

</treqs-link>

<treqs-link type="relatesTo" target="4d8be51c-df13-41e9-ac02-00ffa31c3b80" />
<treqs-link type="relatesTo" target="bc89e02a76c811ebb811cf2f044815f7" />
<treqs-link type="relatesTo" target="638fa22e76c911ebb811cf2f044815f7" />

</treqs-element>

<treqs-element id="0ea4f594a71311efa36cd89ef374263e" type="information">

#### Possible solution for Req-2.11

For the traversal of a file, we XML-escape every line that doesn't include the word treqs* or root, unless it is explicitly marked as an XML comment:

```html
<!-- This is not parsed even though it contains the word treqs-element 
nor is this escaped even though it has the word root -->
```

This allows for embedding treqs-element and treqs-link tags in source files of languages <!-- Note that we need a line break or a better solution for this text-->
that usually uses the ``<`` and ``>`` in their syntax.

This helps with TReqs being more developer-centric.

<treqs-link type="relatesTo" target="ab59f9c4a71111efaf2ed89ef374263e"/>


</treqs-element>