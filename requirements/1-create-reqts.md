# TReqs Create

In this section we describe refined requirements of [Req-1: Create TReqs Elements](treqs-system-requirements.md#req-1-create-treqs-elements).
In addition, two more commands are specified:

- [treqs createlink](#req-17-create-links)
- [treqs generateid](#req-18-generate-ids)

## Detailed requirements for treqs create

<treqs-element id="4c4fbfd250a111edbe22c9328ceec9a7" type="requirement">

### Req-1.1: TReqs create shall provide the following command line interface

```bash
Usage: 
treqs create [OPTIONS]
```

This command creates a treqs element and prints it on the command line.

#### Options

```--type``` The treqs element type that the new element should have. If available, treqs will select a template for this type.

```--label``` A short (less than one line) text describing this treqs element. Markdown (headings) encouraged.

```--amount``` The number of times the element should be created using the given template. Default value is 1.

```--templatefolder``` Location where the templates are stored. Default location is the template folder in the treqs homefolder. A path to any folder can be given here and it is recommended to maintain a template folder for each project in which treqs is used. In that case, consider a template folder in your local repository, e.g. as sub-folder of requirements.

```--template```  A .md file that contains a template for specifying a requirement. This allows to choose a template independent from type.

```--verbose``` Print verbose output instead of only the most important messages.

```--interactive / --non-interactive``` Choose between an interactive or a non-interactive interface.

<treqs-link type="hasParent" target="97a8fb92-9613-11ea-bb37-0242ac130002">

#### Links to

- hasParent: [Req-1: Create TReqs Elements](treqs-system-requirements.md#req-1-create-treqs-elements)

</treqs-link>
</treqs-element>

<treqs-element id="3fe7839e509f11edbe22c9328ceec9a7" type="requirement">

### Req-1.2: Unique IDs

TReqs shall assign a unique ID to each newly created element.

#### Links to

<treqs-link type="hasParent" target="97a8fb92-9613-11ea-bb37-0242ac130002">

- hasParent: [Req-1: Create TReqs Elements](treqs-system-requirements.md#req-1-create-treqs-elements)

</treqs-link>
</treqs-element>

<treqs-element id="4d7ca13c76ae11ebb811cf2f044815f7" type="requirement">

### Req-1.3: TReqs create shall be non-interactive by default

The treqs create command shall be non-interactive by default.
The output will be a treqs element with an undefined type and its content will be defined by the treqs_element template.

#### Links to

<treqs-link type="hasParent" target="97a8fb92-9613-11ea-bb37-0242ac130002">

- hasParent: [Req-1: Create TReqs Elements](treqs-system-requirements.md#req-1-create-treqs-elements)

</treqs-link>
</treqs-element>

<treqs-element id="0d23e196a68811ef84cc8adebfb72d7c" type="information">

#### Rationale for Req-1.3

It is crucial for integrating treqs into scripts and continuous integration setups as well as for power users to execute treqs quickly without being stopped in interactive dialogues.
<treqs-link type="relatesTo" target="4d7ca13c76ae11ebb811cf2f044815f7"/>
</treqs-element>

<treqs-element id="405782b6a68811ef98eb8adebfb72d7c" type="information">

> **Example:** Generate a treqs element with the default type and template
>
>```bash
>treqs create
>```
>
>Since no values were given, the treqs_element.md template in "treqs-ng/templates" will be used.
>
> **Output:**
>
> ```markdown
><treqs-elem id="8e8431404f9c11ed9207f218984a2e78" type="undefined">
>
>
>
>### Treqs element
>
><!-- Use markdown to describe the treqs element. Consider to 
>     use ears template for system requirements, userstory 
>     template for user stories, or planguage template for 
>     quality requirements. -->
>
></treqs-elem>
> ```
>

  <treqs-link type="relatesTo" target="4d7ca13c76ae11ebb811cf2f044815f7"/>
</treqs-element>

<treqs-element id="b09debd2509f11edbe22c9328ceec9a7" type="requirement">

### Req-1.4: TReqs create shall have an interactive mode

In order to change the default behaviour and provide an interactive mode, the optional argument `interactive` can be included.
Then the user will be prompted with stating the type and the content of the treqs element to be created.
The interactive mode should guide new users to create treqs-elements that are useful for their case.
It should offer insights on which options exist.

```bash
treqs create --interactive
Which type should the element have? [undefined]: 
Enter the label for the element []: 
```

#### Links to

<treqs-link type="hasParent" target="97a8fb92-9613-11ea-bb37-0242ac130002">

- hasParent: [Req-1: Create TReqs Elements](treqs-system-requirements.md#req-1-create-treqs-elements)

</treqs-link>
</treqs-element>

<treqs-element id="39f253a076ae11ebb811cf2f044815f7" type="requirement">

### Req-1.5: TReqs create shall allow the use of templates

When creating new treqs-elements, the initial content of the treqs element can be defined based on a template.
The command attempts to locate the correct template file (e.g. based on the treqs element type) and to read the content of the template file and creates a treqs element according to it.
Label, template folder and template are also optional arguments for treqs create.

#### Links to

<treqs-link type="hasParent" target="97a8fb92-9613-11ea-bb37-0242ac130002">

- hasParent: [Req-1: Create TReqs Elements](treqs-system-requirements.md#req-1-create-treqs-elements)
</treqs-link>

> **Examples**
>
>**Note (example output with ``treqs-elem``):**  
>In the following examples, we replace ``treqs-element`` in the output with ``treqs-elem`` to avoid clashes with treqs tooling (the examples would otherwise been shown as requirements in treqs list for this project).
>
>**Example 1.** type and template name is the same, template is stored in the default folder location and interactive interface is used.
>
>```bash
>treqs create --interactive
>Which type should the element have? [undefined]: userstory
>Enter the label for the element []: userstory
> ```
>
>Given these values, the userstory.md template in "treqs-ng/templates" will be used.
>
>**Output:**
>
>```markdown
><treqs-elem id="41088225447711ec968118dbf21d2949" type="userstory">
>
>userstory
>
>### As a : TBD
>### I want : TBD
>### so that: TBD
>
></treqs-elem>
>```
>
>**Example 2.** type and template name is different, template is stored in the default folder location
>
>```bash
>treqs create --type qualityattributes --label QA --template planguage
>```
>
>**Output:**
>
>```markdown
><treqs-elem id="8edfda71447711eca52518dbf21d2949" type="qualityattributes">
>
>QA
>
>### TAG: TBD
>### PLAN: TBD
>### SCALE: TBD
>### MUST: TBD
>### STRETCH: TBD
>### WISH: TBD
>### RECORD: TBD
>### TREND: TBD
>### STAKEHOLDER: TBD
>### AUTHORITY: TBD
>### DEFINED: TBD
>
></treqs-elem>
>
>```
>
>**Example 3:** type and template name the same, template is stored in other location
>
>```bash
>treqs create --type userstory --templatefolder C:\\Users\\SpEeDY\\Desktop             
>```
>
>**Output:**
>
>```markdown
><treqs-elem id="d7f7b73c44a311ec83b118dbf21d2949" type="userstory">
>
>userstory
>
>### As a : TBD
>### I want : TBD
>### so that: TBD
>
></treqs-elem>
>```
>
>**Example 4.** template does not exist for this type, default template is used and a message with additional information will be generated
>
>```bash
>treqs create --type goal
>```
>
>**Output:**
>
>```markdown
><treqs-elem id="fbe4827a4fa211edbcf2f218984a2e78" type="goal">
>
>
>
>### Treqs element
>
><!-- Use markdown to describe the treqs element. Consider to 
>     use ears template for system requirements, userstory 
>     template for user stories, or planguage template for 
>     quality requirements. -->
>
></treqs-elem>
>Template not found for this type. Output generated with default template. Refer to treqs create --help.
>```
>
>**Example 5.** Executing Treqs create with all available parameters
>
>```bash
>treqs create --type goal  --amount 2 --label goal --templatefolder C:\\Users\\SpEeDY\\Desktop --template goal
>```
>
>**Output:**
>
>```markdown
><treqs-elem id="562b082f44a511ec930618dbf21d2949" type="goal">
>
>goal
>
>This is example template for goal
></treqs-elem>
><treqs-elem id="562c658344a511ec91f118dbf21d2949" type="goal">
>
>goal
>
>This is example template for goal
></treqs-elem>
>```

</treqs-element>

<treqs-element id="8dd284e4513c11ed97c08adebfb72d7e" type="information">

### Figure with details on Req-1.5 (create with template)

Treqs create selects the template according to the following schema:

<!--
@startuml treqs-create

start
if (templatefolder given) then (no)
  :set templatefolder = treqs-folder/templates;
endif

if (interactive is set) then (yes)
  :prompt for type;
  :prompt for label;
endif

if (template is set) then (no)
switch (type given) 
case (no)
  :set type = undefined and \nset template = treqs_element.md;
case (yes, but no matching \ntemplate exists)
    :set template = treqs_element.md and\n set warning = no template found;
case (yes, and matching \ntemplate found)
  :set template;
endswitch
endif

:generate id;
:print treqs element according to template, type, and label;
:print warning;  

stop
@enduml
-->
![treqs create details](treqs-create.png)
</treqs-element>

<treqs-element id="58a1dcae50ae11edbe22c9328ceec9a7" type="requirement">

### Req-1.6: TReqs create shall allow to create more than one element at a time

While the default is to create just one element, the optional option `--amount` allows to specify a number of elements that will be created at the same time.

#### Links to

<treqs-link type="hasParent" target="97a8fb92-9613-11ea-bb37-0242ac130002">

- hasParent: [Req-1: Create TReqs Elements](treqs-system-requirements.md#req-1-create-treqs-elements)

</treqs-link>
</treqs-element>

## Related commands (createlink and generateid)

<treqs-element id="9cec8bee512011ed8f118adebfb72d7e" type="requirement">

### Req-1.7: Create links

treqs createlink shall allow to generate treqs links via the createlink command.

<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
</treqs-element>

<treqs-element id="274368b8220011ecb90df018989356c1" type="requirement">

### Req-1.8: Generate IDs

treqs shall allow to batch generate unique IDs via the generateid command.

The reason for this requirement is to facilitate copy+paste style creation of treqs elements, where only IDs need to be replaced.

<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
</treqs-element>
