// This is just an example of a language where '<' and '>' are used in the
// language syntax
// <treqs-element type="unittest" id="sample-id">
// My Contract Validator
public <T, F> ContractValidator<T extends Contract> T validate(Validator<T>, F object) {
    return "This is not valid java program, just an example"
}
// </treqs-element>
