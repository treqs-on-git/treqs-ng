#This is supposed to imitate a python test class as test data to check if treqs-elements are found.
#Not using actual unittest code to avoid the class being executed as a part of a discovery call.
class DummyClass():

    # <treqs-element id="9c0adc12c79211ebb9cbf018989356c1" type="unittest">
    # Does nothing - test data.
    # <treqs-link type="tests" target="c5ae0c10c79211eb9631f018989356c1" />
    # </treqs-element>
    def tests_nothing(self):
        return 0
        
if __name__ == "__main__":
    print("NOP")